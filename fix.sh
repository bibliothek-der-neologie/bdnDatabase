#!/usr/bin/env bash

sed -i "s/http:\/\/sru.gbv.de\/gvk/\/gvk/" "src/main/webapp/js/modals.coffee"
sed -i "s/http:\/\/localhost:9090/\/bdndb/" "src/main/webapp/js/modals.coffee"
sed -i "s/http:\/\/lod.b3kat.de\/data\/title/\/bvb/" "src/main/webapp/js/modals.coffee"
sed -i "s/http:\/\/localhost:3030\/ds\/query/\/fuseki/" "src/main/webapp/js/main.coffee"
sed -i "s/http:\/\/localhost:3030\/ds\/query/\/fuseki/" "src/main/webapp/js/modals.coffee"

