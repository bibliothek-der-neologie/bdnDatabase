/**
 * Copyright (c) 2014–2015 Hannes Riebl
 * Copyright (c) 2019 Michelle Weidling
 * Copyright (c) 2019–2020 Stefan Hynek
 * 
 * This file is part of bdnDatabase.
 * 
 * bdnDatabase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * bdnDatabase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with bdnDatabase.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.github.hriebl.bdnDatabase;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.jena.query.DatasetAccessor;
import org.apache.jena.query.DatasetAccessorFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

class FusekiCRUD
{
  DatasetAccessor accessor;

  public FusekiCRUD ()
  { // fuseki uri is hardcoded, to be outsourced to config file
    accessor = DatasetAccessorFactory.createHTTP("http://localhost:8081/fuseki/ds/data");
  }

  public void create(String tgURI, String rdfXML)
  {
    InputStream inStream = new ByteArrayInputStream(rdfXML.getBytes());
    Model model = ModelFactory.createDefaultModel();
    model.read(inStream, null, "RDF/XML");
    accessor.add(tgURI, model);
  }

  public void update(String tgURI, String rdfXML)
  {
    this.delete(tgURI);
    this.create(tgURI, rdfXML);
  }

  public void delete(String tgURI)
  {
    accessor.deleteModel(tgURI);
  }
}
