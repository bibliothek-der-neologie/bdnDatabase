/**
 * Copyright (c) 2014–2015 Hannes Riebl
 * Copyright (c) 2019 Michelle Weidling
 * Copyright (c) 2019–2020 Stefan Hynek
 * 
 * This file is part of bdnDatabase.
 * 
 * bdnDatabase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * bdnDatabase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with bdnDatabase.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.github.hriebl.bdnDatabase;

import info.textgrid.namespaces.metadata.core._2010.GenericType;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.core._2010.ProvidedType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.*;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Holder;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.Normalizer;
import java.util.HashSet;
import java.util.Set;

@Path("/")
public class Server {

    private FusekiCRUD fusekiCRUD;
    private TGCrudService tgCRUD;

    public Server()
            throws MalformedURLException {

        fusekiCRUD = new FusekiCRUD();
        tgCRUD = TGCrudClientUtilities.getTgcrud("https://textgridlab.org/1.0/tgcrud/TGCrudService");

    }

    @POST
    @Path("/create")
    @Produces("application/ld+json")
    public String create(@FormParam("tgSID") String tgSID,
                         @FormParam("tgPID") String tgPID,
                         @FormParam("title") String title)
            throws AuthFault,
                   IoFault,
                   MetadataParseFault,
                   ObjectNotFoundFault,
                   UnsupportedEncodingException {

        ProvidedType providedMeta = new ProvidedType();
        providedMeta.setFormat("text/tg.inputform+rdf+xml");
        providedMeta.getTitle().add(title);

        GenericType genericMeta = new GenericType();
        genericMeta.setProvided(providedMeta);

        ObjectType objectMeta = new ObjectType();
        objectMeta.setGeneric(genericMeta);

        MetadataContainerType metaContainer = new MetadataContainerType();
        metaContainer.setObject(objectMeta);

        Holder<MetadataContainerType> tgMeta = new Holder<MetadataContainerType>();
        tgMeta.value = metaContainer;

        String string = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"/>";
        DataSource dataSource = new ByteArrayDataSource(string.getBytes("UTF-8"), "UTF-8");
        DataHandler tgData = new DataHandler(dataSource);

        tgCRUD.create(tgSID, "", null, false, tgPID, tgMeta, tgData);

        genericMeta = tgMeta.value.getObject().getGeneric();
        String uri = genericMeta.getGenerated().getTextgridUri().getValue();
        uri = uri.replaceFirst("\\.[0-9]*$", "");

        return "{\"@id\": \"" + uri + "\"}";

    }

    @GET
    @Path("/read/{tgURI}")
    @Produces("text/turtle")
    public String read(@PathParam("tgURI") String tgURI,
                       @QueryParam("tgSID") String tgSID)
            throws AuthFault,
                   IOException,
                   IoFault,
                   MetadataParseFault,
                   ObjectNotFoundFault,
                   ProtocolNotImplementedFault {

        Holder<MetadataContainerType> tgMeta = new Holder<MetadataContainerType>();
        Holder<DataHandler> tgData = new Holder<DataHandler>();

        tgCRUD.read(tgSID, "", tgURI, tgMeta, tgData);

        InputStream inStream = tgData.value.getInputStream();

        Model model = ModelFactory.createDefaultModel();

        model.read(inStream, null, "RDF/XML");

        OutputStream outStream = new ByteArrayOutputStream();
        model.write(outStream, "TURTLE");

        return outStream.toString();

    }

    @POST
    @Path("/update/{tgURI}")
    @Consumes("application/ld+json")
    public Response write(@PathParam("tgURI") String tgURI,
                          @QueryParam("tgSID") String tgSID,
                          String jsonLD)
            throws AuthFault,
                   IOException,
                   IoFault,
                   MetadataParseFault,
                   ObjectNotFoundFault,
                   UpdateConflictFault {

        MetadataContainerType metaContainer = tgCRUD.readMetadata(tgSID, "", tgURI);
        Holder<MetadataContainerType> tgMeta = new Holder<MetadataContainerType>();
        tgMeta.value = metaContainer;

        jsonLD = Normalizer.normalize(jsonLD, Normalizer.Form.NFC);
        InputStream inStream = new ByteArrayInputStream(jsonLD.getBytes());

        Model model = ModelFactory.createDefaultModel();
        model.read(inStream, null, "JSON-LD");

        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        model.write(outStream, "RDF/XML");
        byte[] byteArray = outStream.toByteArray();

        DataSource dataSource = new ByteArrayDataSource(byteArray, "UTF-8");
        DataHandler tgData = new DataHandler(dataSource);

        tgCRUD.update(tgSID, "", tgMeta, tgData);

        String tgPID = tgMeta.value.getObject().getGeneric().getGenerated().getProject().getId();

        Set<String> pidSet = new HashSet<String>();
        //hard coded Project IDs to be outsourced to configuration file
        pidSet.add("TGPR-60c6a362-f1f6-1c81-544b-53837e6c4fb1"); // Neologie
        pidSet.add("TGPR-2a20c4b9-3454-fc0c-0827-54f02b4fc530"); // Neologie RDF Instanzen
        pidSet.add("TGPR-1d223cd7-427b-7c71-c1db-54f01f8072d8"); // Neologie RDF Personen
        pidSet.add("TGPR-0ab1b52d-e8d6-1979-d747-54f021783406"); // Neologie RDF Werke

        if (pidSet.contains(tgPID)) {
            String rdfXML = new String(byteArray, "UTF-8");
            fusekiCRUD.update(tgURI, rdfXML);
        }

        return Response.ok().build();

    }

    @DELETE
    @Path("/delete/{tgURI}")
    public Response delete(@PathParam("tgURI") String tgURI,
                           @QueryParam("tgSID") String tgSID)
            throws AuthFault,
                   IoFault,
                   ObjectNotFoundFault,
                   RelationsExistFault {

        tgCRUD.delete(tgSID, "", tgURI);
        fusekiCRUD.delete(tgURI);

        return Response.ok().build();

    }

    @GET
    @Path("/swb")
    @Produces("application/xml")
    public String swb(@QueryParam("query") String query)
            throws IOException,
                   MalformedURLException,
                   TransformerException {

      TransformerFactory factory = TransformerFactory.newInstance();

      query = query.replaceAll(" ", "+");

      /* OCLCPICA Picarta database
       * alter Query-uri
       * DB=2.1
       * username=
       * password=
       * maximumRecords=999
       * operation=searchRetrieve
       * recordSchema=marc21
       * query=pica.all%3D%22" + query + "%22"
       *
       * neuer query-uri:
       * query=.+%3D+%22test%22
       * version=1.1
       * operation=searchRetrieve
       * om: stylesheet=http%3A%2F%2Fswb.bsz-bw.de%2Fsru%2FDB%3D2.1%2F%3Fxsl%3DsearchRetrieveResponse
       * recordSchema=marc21
       * maximumRecords=999
       * om: startRecord=1
       * om: recordPacking=xml
       * om: sortKeys=none
       * om: x-info-5-mg-requestGroupings=none
       *
       * alte Definition:
       * URL xmlURL = new URL("http://swb.bsz-bw.de/sru/DB=2.1/username=/" +
       *   "password=/?maximumRecords=999&operation=searchRetrieve&" +
       *   "recordSchema=marc21&query=pica.all%3D%22" + query + "%22");
       */
      URL xmlURL = new URL("http://swb.bsz-bw.de/sru/?query=pica.all+%3D+%22" + query + "%22&version=1.1&operation=searchRetrieve&recordSchema=marc21&maximumRecords=50");

      String xmlString = IOUtils.toString(xmlURL.openStream());
      xmlString = xmlString.replaceFirst("(srw:searchRetrieveResponse)",
          "$1 xmlns=\"http://www.loc.gov/MARC21/slim\"");

      InputStream xmlStream = new ByteArrayInputStream(xmlString.getBytes());
      Source xmlSource = new StreamSource(xmlStream);

      //hard coded service to be outsourced into config file
      URL xslURL = new URL("http://localhost:8082/bdndb/marc2mods.xsl");
      Source xslSource = new StreamSource(xslURL.openStream());

      Transformer transformer = factory.newTransformer(xslSource);

      OutputStream stream = new ByteArrayOutputStream();
      StreamResult result = new StreamResult(stream);

      transformer.transform(xmlSource, result);

      return stream.toString();

    }

}
