###
Copyright (c) 2014–2015 Hannes Riebl
Copyright (c) 2019 Michelle Weidling
Copyright (c) 2019–2020 Stefan Hynek

This file is part of bdnDatabase.

bdnDatabase is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnDatabase is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnDatabase.  If not, see <https://www.gnu.org/licenses/>.
###

tgf = new tgForms

caret = "<span class=\"caret\"></span>"
tgURI = ""
tgSID = ""
type = ""


###############
## Functions ##
###############

addLinkInfo = ->
  $("div[data-tgforms-name=\"bf:instanceOf\"]").each(->
    $this = $(this)
    value = $this.find("span.value").text()

    if not value
      return true

    request = {
      "output": "json",
      "query": "PREFIX dc: <http://purl.org/dc/elements/1.1/> \n
               PREFIX textgrid: <http://textgridrep.de/textgrid:> \n
               SELECT ?o \n
               { \n
               GRAPH ?g \n
               { \n" +
               value + " dc:title ?o . \n
               } \n
               }"
    }

    $.ajax(
      type: "GET",
      url: "/fuseki",
      cache: false,
      data: request
    )
    .done((data) ->
      title = data["results"]["bindings"][0]["o"]["value"]
      $this.find("span.value").after("<span>[ " + title + " ]</span>")
    )
  )

  $("div[data-tgforms-name=\"dct:creator\"],
      div[data-tgforms-name=\"dct:publisher\"]").each(->
    $this = $(this)
    value = $this.find("span.value").text()

    if not value
      return true

    request = {
      "output": "json",
      "query": "PREFIX foaf: <http://xmlns.com/foaf/0.1/> \n
               PREFIX textgrid: <http://textgridrep.de/textgrid:> \n
               SELECT ?o \n
               { \n
               GRAPH ?g \n
               { \n" +
               value + " foaf:name ?o . \n
               } \n
               }"
    }

    $.ajax(
      type: "GET",
      url: "/fuseki",
      cache: false,
      data: request
    )
    .done((data) ->
      name = data["results"]["bindings"][0]["o"]["value"]
      $this.find("span.value").after("<span>[ " + name + " ]</span>")
    )
  )

addReferences = ->
  $("div#references").empty()

  request = {
    "output": "json",
    "query": "PREFIX bf: <http://bibframe.org/vocab/>
             PREFIX dc: <http://purl.org/dc/elements/1.1/>
             PREFIX textgrid: <http://textgridrep.de/textgrid:>
             SELECT ?s ?t1 ?w ?d ?t2
             {
               GRAPH ?g1
               {
                 ?s ?p " + tgURI + " .
                 OPTIONAL
                 {
                   ?s dc:title ?t1 .
                 }
                 OPTIONAL
                 {
                   ?s bf:instanceOf ?w .
                   ?s dc:date ?d .
                   GRAPH ?g2
                   {
                     ?w dc:title ?t2 .
                   }
                 }
               }
             }"
  }

  $.ajax(
    type: "GET",
    url: "/fuseki",
    cache: false,
    data: request
  )
  .done((data) ->
    for binding in data["results"]["bindings"]
      if binding["t1"]
        t1 = binding["t1"]["value"]
        s = binding["s"]["value"]
        s = s.replace(/^.*\//, "")

        $("div#references").append(
          "<p>
            <strong>Autor des Werks</strong>: " +
            t1 + " [ " + s + " ]
          </p>"
        )
      else
        t2 = binding["t2"]["value"]
        d = binding["d"]["value"]
        w = binding["w"]["value"]
        w = w.replace(/^.*\//, "")

        $("div#references").append(
          "<p>
            <strong>Herausgeber der Ausgabe</strong>: " +
            t2 +  " (" + d + ")" + " [ " + w + " ]
          </p>"
        )
  )

buildForm = ->
  tgf.buildForm(type, "div#content")
  tgf.fillForm(tgURI, "div#content")

  if type is "foaf:Person"
    tgf.buildForm("bio:Birth", "div#birth-form")
    tgf.fillForm(tgURI + "#birth", "div#birth-form")
    $("div.bio\\:birth input").val(tgURI + "#birth")
    $("div#birth").show()

    tgf.buildForm("bio:Death", "div#death-form")
    tgf.fillForm(tgURI + "#death", "div#death-form")
    $("div.bio\\:death input").val(tgURI + "#death")
    $("div#death").show()

    addReferences()
  else
    $("div#birth").hide()
    $("div#death").hide()

  addLinkInfo()
  setLinks()

log = (val) ->
  $("div#log").append("<p>" + val + "</p>")

open = (val) ->
  return if type

  tgURI = val.replace(/\.[0-9]*$/, "")
  $("h1").text(tgURI)

  $.ajax(
    type: "GET",
    url: "db/read/" + tgURI + "?tgSID=" + tgSID,
    mimeType: "text/turtle",
    cache: false
  )
  .done((data) ->
    tgf.addTurtle(data, ->
      type = tgf.getType(tgURI)
      updateTypeButton()
      buildForm()
    )
  )

save = ->
  content = tgf.getInput(tgURI, type, "div#content")
  data = [content]

  if type is "foaf:Person"
    birth = tgf.getInput(tgURI + "#birth", "bio:Birth", "div#birth-form")
    death = tgf.getInput(tgURI + "#death", "bio:Death", "div#death-form")
    data.push(birth, death)

  data = JSON.stringify(data)

  $.ajax(
    type: "POST",
    url: "db/update/" + tgURI + "?tgSID=" + tgSID,
    contentType: "application/ld+json",
    data: data
  )
  .done(->
    $("div#success").show("slow")
    saveCall = -> $("div#success").hide("slow")
    setTimeout(saveCall, 5000)
  )
  .fail(->
    $("div#fail").show("slow")
    saveCall = -> $("div#fail").hide("slow")
    setTimeout(saveCall, 5000)
  )

setLinks = ->
  $("span.value").each(->
    if not $(this).text()
      return true

    prefix = $(this).text().replace(/:.*$/, "")
    id = $(this).text().replace(/^.*:/, "")
    href = tgf.getPrefixes()[prefix] + id

    if prefix isnt "textgrid" and prefix isnt "vd18"
      $(this).wrap("<a href=\"" + href + "\" target=\"_blank\"></a>")
  )

setSid = (val) ->
  tgSID = val

updateTypeButton = ->
  if type is "bibo:Article"
    $("button#type-button").html("Unselbständige Ausgabe " + caret)
  else if type is "bibo:Book"
    $("button#type-button").html("Selbständige Ausgabe " + caret)
  else if type is "foaf:Person"
    $("button#type-button").html("Person " + caret)
  else
    $("button#type-button").html("Werk " + caret)


#################
## Interaction ##
#################

$("a#article").click(->
  type = "bibo:Article"
  updateTypeButton()
  buildForm()
)

$("a#book").click(->
  type = "bibo:Book"
  updateTypeButton()
  buildForm()
)

$("a#person").click(->
  type = "foaf:Person"
  updateTypeButton()
  buildForm()
)

$("a#work").click(->
  type = "bf:Work"
  updateTypeButton()
  buildForm()
)

$("button#delete").click(->
  $.ajax(
    type: "DELETE",
    url: "db/delete/" + tgURI + "?tgSID=" + tgSID
  )
  .done(->
    window.close()
  )
)

$("button#save").click(->
  save()
)


#########
## Run ##
#########

$.ajax(
  type: "GET",
  url: "bdnDatabase.ttl",
  mimeType: "text/turtle"
)
.done((data) ->
  tgf.addTurtle(data, -> true)
)
