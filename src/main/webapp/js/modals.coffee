###
Copyright (c) 2014–2015 Hannes Riebl
Copyright (c) 2019 Michelle Weidling
Copyright (c) 2019–2020 Stefan Hynek

This file is part of bdnDatabase.

bdnDatabase is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bdnDatabase is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bdnDatabase.  If not, see <https://www.gnu.org/licenses/>.
###

ajaxRequests = []
datasetTarget = ''
field = ''


modsToRadio = (mods) ->
  id = $(mods).find('recordInfo > recordIdentifier').text().trim()

  author = ''

  $(mods).children('name[usage="primary"]').each(->
    if $(this).children('namePart:not([type])').length
      author += $(this).children('namePart:not([type])').text().trim() + '. '
    else
      author += $(this).children('namePart[type="given"]').text().trim() + ' '
      author += $(this).children('namePart[type="family"]').text().trim() + '. '
  )

  title = ''

  $(mods).find('titleInfo > *').each(->
    title += $(this).text().trim() + '. '
  )

  edition = ''

  $(mods).find('originInfo > edition').each(->
    edition += $(this).text().trim() + '. '
  )

  place = ''

  $(mods).find('originInfo placeTerm[type="text"]').each(->
    place += $(this).text().trim() + '. '
  )

  date = ''

  $(mods).find('originInfo > dateIssued').each(->
    date += $(this).text().trim() + '. '
  )

  extent = ''

  $(mods).find('physicalDescription > extent').each(->
    extent += $(this).text().trim() + '. '
  )

  publisher = ''

  $(mods).find('originInfo > publisher').each(->
    publisher += $(this).text().trim() + '. '
  )

  return(
      '<div class="radio">
        <label>
          <input type="radio" name="dataset-radios" value="' + id + '">
          <strong>' + author + '</strong>
          <span class="title">' + title + '</span>
          <span class="edition">' + edition + '</span>
          <span class="place">' + place + '</span>
          <span class="date">' + date + '</span>
          <span class="extent hidden">' + extent + '</span>
          <span class="publisher hidden">' + publisher + '</span>
        </label>
      </div>'
  )


###################
## Dataset modal ##
###################

$('div#content').on('click', 'div.owl\\:sameAs button', ->
  field = $(this).closest('div.form-group')
  $('div#dataset-modal').modal('show')
)

$('ul#dataset-target-menu a').click((e) ->
  e.preventDefault()

  $this = $(this)
  datasetTarget = $this.attr('id')

  $('button#dataset-target-button').html($this.text() + ' ' + caret)

  ajaxRequest.abort() for ajaxRequest in ajaxRequests
  $('input#dataset-search-input').prop('disabled', false)
  $('button#dataset-search-button').prop('disabled', false)
  $('button#dataset-search-button').text('Suchen');
  $('div#dataset-results').empty()
)

$('a#bvb').click((e) ->
  e.preventDefault()

  ajaxRequest.abort() for ajaxRequest in ajaxRequests
  $('input#dataset-search-input').prop('disabled', true);
  $('button#dataset-search-button').prop('disabled', true);
  $('button#dataset-search-button').text('Suchen');

  $('div#dataset-results').html(
      '<p style="margin-top: 10px;">BVB-Katalogschlüssel von
        <a href="//gateway-bayern.de" target="_blank">gateway-bayern.de</a>:
      </p>
      <input type="text" class="form-control"></input>'
  )
)

$('a#vd18').click((e)->
  e.preventDefault()

  ajaxRequest.abort() for ajaxRequest in ajaxRequests
  $('input#dataset-search-input').prop('disabled', true);
  $('button#dataset-search-button').prop('disabled', true);
  $('button#dataset-search-button').text('Suchen');

  $('div#dataset-results').html(
      '<p style="margin-top: 10px;">VD18-Schlüssel bspw. von
        <a href="//vd18.de" target="_blank">vd18.de</a>:
      </p>
      <input type="text" class="form-control"></input>'
  )
)

$('button#dataset-search-button').click((e) ->
  e.preventDefault()

  $this = $(this)
  # $this.button('loading')
  $this.prop('disabled', true)
  $this.text('Lade…')

  $('div#dataset-results').empty()

  if datasetTarget is ''
    $('div#dataset-results').html(
        '<p style="margin-top: 10px;">Bitte Suchziel wählen!</p>'
    )

    # $this.button('reset')
    $this.prop('disabled', false)
    $this.text('Suchen')

  # CT

  if datasetTarget is 'ct'
    keyword = $('input#dataset-search-input').val()

    request = {
      'operation': 'searchRetrieve',
      'query': 'ct.personalName="' + keyword + '"',
      'version': 1.2
    }

    ajaxRequests.push($.ajax(
      type: 'GET',
      url: 'https://data.cerl.org/thesaurus/_sru',  # protocol update: The CERL Thesaurus SRU interface currently supports the protocol version 1.2; specification: https://www.loc.gov/standards/sru/sru-1-2.html
      data: request
    )
    .done((data) ->
      results = $(data).find('srw\\:recordData, recordData')

      $(results).each((key, value) ->
        id = $(value).find('record').attr('id')

        name = $(value).find('display').text()
        bio = $(value).find('biographicalData').text()

        $('div#dataset-results').append(
            '<div class="radio">
              <label>
                <input type="radio" name="dataset-radios" value="' + id + '">
                <strong>' + name + ':</strong> ' + bio +
              '</label>
            </div>'
        )
      )

      # $this.button('reset')
      $this.prop('disabled', false)
      $this.text('Suchen')
    ))

  # DNB

  if datasetTarget is 'dnb'
    keyword = encodeURIComponent($('input#dataset-search-input').val())

    ajaxRequests.push($.ajax(
      type: 'GET',
      url: 'https://ref.dariah.eu/beta/pndsearch/pndquery.xql?ln=' + keyword
    )
    .done((data) ->
      results = $(data).find('person')

      $(results).each((key, value) ->
        id = $(value).attr('id')

        name = $(value).find('preferred_name').text()
        info = $(value).find('info').text()

        if $('div#dataset-results input[value="' + id + '"]').length
          return true

        $('div#dataset-results').append(
            '<div class="radio">
              <label>
                <input type="radio" name="dataset-radios" value="' + id + '">
                <strong>' + name + ':</strong> ' + info +
              '</label>
            </div>'
        )
      )

      # $this.button('reset')
      $this.prop('disabled', false)
      $this.text('Suchen')
    ))

  # GVK

  if datasetTarget is 'gvk'
    keyword = $('input#dataset-search-input').val()

    request = {
      'maximumRecords': 999,
      'operation': 'searchRetrieve',
      'query': keyword,
      'recordSchema': 'mods',
      'version': 1.1
    }

    ajaxRequests.push($.ajax(
      type: 'GET',
      url: '/gvk',
      data: request
    )
    .done((data) ->
      # $this.button('reset')
      $this.prop('disabled', false)
      $this.text('Suchen')

      results = $(data).find('mods')

      $(results).each((key, value) ->
        $('div#dataset-results').append(modsToRadio(value))
      )
    ))

  # SWB

  if datasetTarget is 'swb'
    keyword = $('input#dataset-search-input').val()

    ajaxRequests.push($.ajax(
      type: 'GET',
      url: '/bdndb/db/swb?query=' + keyword
    )
    .done((data) ->
      # $this.button('reset')
      $this.prop('disabled', false)
      $this.text('Suchen')

      results = $(data).find('mods')

      $(results).each((key, value) ->
        $('div#dataset-results').append(modsToRadio(value))
      )
    ))
)

$('button#dataset-set').click((e) ->
  e.preventDefault()

  input = $('div#dataset-results input[name="dataset-radios"]:checked')

  # BVB

  if datasetTarget is 'bvb'
    id = $('div#dataset-results input').val()

    $.ajax(
      type: 'GET',
      url: '/bvb/' + id + '?output=xml'
    )
    .done((data) ->
      field.find('span.value').text('bvb:' + id)
      field.find('span.value').wrap('<a href="' + tgf.getPrefixes()['bvb'] +
          id + '" target="_blank"></a>')

      title = $(data).find('dc\\:title, title').text().trim()
      edition = $(data).find('bibo\\:edition, edition').text().trim()
      extent = $(data).find('dcterms\\:extent, extent').text().trim()

      # responsibility = $(data).find(
      #     'dcterms\\:description, description'
      # ).text().trim()

      publication = $(data).find(
          'rdagr1\\:publicationStatement, publicationStatement'
      ).text().trim()

      date = $(data).find('dcterms\\:issued, issued').text().trim()

      $('div.dc\\:title input').val(title)
      $('div.bf\\:edition input').val(edition)
      $('div.dct\\:extent input').val(extent)
      $('div.bf\\:providerStatement input').val(publication)
      # $('div.bf\\:responsibilityStatement input').val(responsibility)
      $('div.dc\\:date input').val(date)
    )

  # CT

  if input.length and datasetTarget is 'ct'
    field.find('span.value').text('ct:' + input.val())
    field.find('span.value').wrap('<a href="' + tgf.getPrefixes()['ct'] +
        input.val() + '" target="_blank"></a>')

  # DNB

  if input.length and datasetTarget is 'dnb'
    field.find('span.value').text(input.val().replace(/^pnd:/, 'dnb:'))
    field.find('span.value').wrap('<a href="' + tgf.getPrefixes()['dnb'] +
        input.val().replace(/^pnd:/, '') + '" target="_blank"></a>')

  # CT & DNB

  if input.length and (datasetTarget is 'ct' or datasetTarget is 'dnb')
    strong = input.closest('label').children('strong')
    firstName = strong.text().replace(/^.*, /, '').replace(/:$/, '')
    lastName = strong.text().replace(/,.*$/, '')

    $('div.foaf\\:name input').val(firstName + ' ' + lastName)

  # GVK

  if input.length and datasetTarget is 'gvk'
    field.find('span.value').text('gvk:' + input.val())
    field.find('span.value').wrap('<a href="' + tgf.getPrefixes()['gvk'] +
        input.val() + '" target="_blank"></a>')

  # SWB

  if input.length and datasetTarget is 'swb'
    field.find('span.value').text('swb:' + input.val())
    field.find('span.value').wrap('<a href="' + tgf.getPrefixes()['swb'] +
        input.val() + '" target="_blank"></a>')

  # GVK & SWB

  if input.length and (datasetTarget is 'gvk' or datasetTarget is 'swb')
    title = input.closest('label').children('span.title').text()
      .replace(/. $/, '')

    edition = input.closest('label').children('span.edition').text()
        .replace(/. $/, '')

    extent = input.closest('label').children('span.extent').text()
        .replace(/. $/, '')

    place = input.closest('label').children('span.place').text()
        .replace(/. $/, '')

    date = input.closest('label').children('span.date').text()
        .replace(/. $/, '')

    publisher = input.closest('label').children('span.publisher').text()
        .replace(/. $/, '')

    $('div.dc\\:title input').val(title)
    $('div.bf\\:edition input').val(edition)
    $('div.dct\\:extent input').val(extent)
    $('div.bf\\:providerStatement input').val(place + ' : ' + publisher + ' ' + date)
    # $('div.bf\\:responsibilityStatement input').val(publisher + ' (Hrsg.)')
    $('div.dc\\:date input').val(date)

  # VD18

  if $('div#dataset-results input').val().length and datasetTarget is 'vd18'
    id = $('div#dataset-results input').val()
    field.find('span.value').text('vd18:' + id)

  $('div#dataset-modal').modal('hide')
)


##################
## Person modal ##
##################

$('div#content').on('click', 'div.dct\\:creator button,
    div.dct\\:publisher button', ->
  field = $(this).closest('div.form-group')
  $('div#person-modal').modal('show')
)

$('button#person-search-button').click((e) ->
  e.preventDefault()

  $this = $(this)
  # $this.button('loading')
  $this.prop('disabled', true)
  $this.text('Lade…')

  $('div#person-results').empty()
  keyword = $('input#person-search-input').val()

  request = {
    'output': 'json',
    'query': 'PREFIX foaf: <http://xmlns.com/foaf/0.1/> \n
             SELECT ?g ?o \n
             { \n
             GRAPH ?g \n
             { \n
             ?s foaf:name ?o . \n
             FILTER regex(?o, \n
                    replace("' + keyword + '", " ", ".*"), \n
                    "i") \n
             } \n
             }'
  }

  $.ajax(
    type: 'GET',
    url: '/fuseki',
    cache: false,
    data: request
  )
  .done((data) ->
    results = data['results']['bindings']

    for result in results
      id = result['g']['value']
      name = result['o']['value']

      $('div#person-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="person-radios" value="' + id + '">
              <strong>' + name + '</strong>
            </label>
          </div>'
      )

    # $this.button('reset')
    $this.prop('disabled', false)
    $this.text('Suchen')
  )
)

$('button#person-set').click((e) ->
  e.preventDefault()
  id = $('div#person-results input[name="person-radios"]:checked').val()
  name = $('div#person-results input[value="' + id + '"] + strong').text()

  if id
    field.find('span.value + span').remove()
    field.find('span.value').text(id).after('<span>[ ' + name + ' ]</name>')

  $('div#person-modal').modal('hide')
)


#################
## Place modal ##
#################

$('div#birth-form').on('click', 'div.bio\\:place button', ->
  field = $(this).closest('div.form-group')
  $('div#place-modal').modal('show')
)

$('div#death-form').on('click', 'div.bio\\:place button', ->
  field = $(this).closest('div.form-group')
  $('div#place-modal').modal('show')
)

$('button#place-search-button').click((e) ->
  e.preventDefault()

  $this = $(this)
  # $this.button('loading')
  $this.prop('disabled', true)
  $this.text('Lade…')

  $('div#place-results').empty()
  keyword = encodeURIComponent($('input#place-search-input').val())

  $.ajax(
    type: 'GET',
    url: 'http://geobrowser.de.dariah.eu/tgnsearch/tgnquery2.xql?ac=' + keyword
  )
  .done((data) ->
    results = $(data).find('term')

    $(results).each((key, value) ->
      id = $(value).attr('id')

      name = $(value).find('name').text()
      path = $(value).find('path').text()

      $('div#place-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="place-radios" value="' + id + '">
              <strong>' + name + ':</strong> ' + path +
            '</label>
          </div>'
      )
    )

    # $this.button('reset')
    $this.prop('disabled', false)
    $this.text('Suchen')
  )
)

$('button#place-set').click((e) ->
  e.preventDefault()

  id = $('div#place-results input[name="place-radios"]:checked').val()

  if id
    field.find('span.value').text(id)
    field.find('span.value').wrap('<a href="' + tgf.getPrefixes()['tgn'] +
        id.replace(/^tgn:/, '') + '" target="_blank"></a>')

  $('div#place-modal').modal('hide')
)


################
## Work modal ##
################

$('div#content').on('click', 'div.bf\\:instanceOf button', ->
  field = $(this).closest('div.form-group')
  $('div#work-modal').modal('show')
)

$('button#work-search-button').click((e) ->
  e.preventDefault()

  $this = $(this)
  # $this.button('loading')
  $this.prop('disabled', true)
  $this.text('Lade…')

  $('div#work-results').empty()
  keyword = $('input#work-search-input').val()

  request = {
    'output': 'json',
    'query': 'PREFIX bf: <http://bibframe.org/vocab/>
             PREFIX dc: <http://purl.org/dc/elements/1.1/>
             PREFIX dct: <http://purl.org/dc/terms/>
             PREFIX foaf: <http://xmlns.com/foaf/0.1/>
             SELECT ?g1 ?o ?n
             {
               GRAPH ?g1
               {
                 ?s a bf:Work .
                 ?s dc:title ?o .
                 FILTER regex(?o,
                              replace("' + keyword + '", " ", ".*"),
                              "i")
                 OPTIONAL
                 {
                   ?s dct:creator ?c .
                   GRAPH ?g2
                   {
                     ?c foaf:name ?n .
                   }
                 }
               }
             }'
  }

  $.ajax(
    type: 'GET',
    url: '/fuseki',
    cache: false,
    data: request
  )
  .done((data) ->
    results = data['results']['bindings']

    for result in results
      id = result['g1']['value']
      title = result['o']['value']

      if result['n']
        name = result['n']['value']
      else
        name = 'Kein Autor'

      $('div#work-results').append(
          '<div class="radio">
            <label>
              <input type="radio" name="work-radios" value="' + id + '">
              <strong>' + name + '</strong>: ' + title + '
            </label>
          </div>'
      )

    # $this.button('reset')
    $this.prop('disabled', false)
    $this.text('Suchen')
  )
)

$('button#work-set').click((e) ->
  e.preventDefault()
  id = $('div#work-results input[name="work-radios"]:checked').val()
  title = $('div#work-results input[value="' + id + '"] + strong').text()

  if id
    field.find('span.value + span').remove()
    field.find('span.value').text(id).after('<span>[ ' + title + ' ]</span>')

  $('div#work-modal').modal('hide')
)
