#!/usr/bin/env bash

SCRIPT=$(readlink -f "$0")
DIR=$(dirname "$SCRIPT")

FUSEKI_PID="${DIR}/fuseki.pid"
JETTY_PID="${DIR}/jetty.pid"

FUSEKI_LOG="${DIR}/fuseki.log"
JETTY_LOG="${DIR}/jetty.log"

if [[ "$1" == prepare ]]; then

  ### Create Fuseki directory, get dependencies, and compile CoffeeScript

  cd "$DIR"

  if [[ ! -d DB ]]; then
    mkdir DB
  fi

  cd src/main/webapp

  rm -rf node_modules
  npm --prefix . install popper.js bootstrap jquery mustache n3

  rm -rf tgForms
  git clone https://github.com/hriebl/tgForms.git

  cd js

  coffee -bc *.coffee

  cd ../node_modules/n3

  npm install
  npm run browser

  cd ../../tgForms

  wget https://dl.google.com/closure-compiler/compiler-latest.zip
  unzip -nx compiler-latest.zip
  cake build #minify

elif [[ "$1" == start ]]; then

  ### Start Fuseki and Jetty

  cd "$DIR"

  if [[ -f "$FUSEKI_PID" || -f "$JETTY_PID" ]]; then
    echo "bdnDatabase is already running."
    exit 1
  fi

  fuseki-server --update --loc "${DIR}/DB" /ds >> "$FUSEKI_LOG" 2>&1 &
  echo "$!" > "$FUSEKI_PID"

  mvn -Djetty.port=9090 jetty:run >> "$JETTY_LOG" 2>&1 &
  echo "$!" > "$JETTY_PID"

elif [[ "$1" == stop ]]; then

  ### Stop Fuseki and Jetty

  cd "$DIR"

  if [[ ! -f "$FUSEKI_PID" || ! -f "$JETTY_PID" ]]; then
    echo "bdnDatabase is not running."
    exit 1
  fi

  pkill -P $(cat "$FUSEKI_PID")
  rm "$FUSEKI_PID"

  kill $(cat "$JETTY_PID")
  rm "$JETTY_PID"

else

  echo "No argument given."
  exit 1

fi
