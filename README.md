bdnDatabase
===========

> Database for persons and bibliographic references in the Bibliothek der Neologie project

bdnDatabase is designed for the use with the [TextGrid](http://www.textgrid.de) RDF Object Editor. It uses [tgForms](https://github.com/hriebl/tgForms), a JavaScript library to generate HTML forms from Turtle RDF representations.

Usage
-----

If you have [node.js](http://nodejs.org), [Bower](http://bower.io) and [Apache Maven](http://maven.apache.org) installed, you can simply run the start script:

```sh
$ ./run.sh
```

Now, you can configure the TextGrid RDF Object Editor to use [http://localhost:9090](http://localhost:9090).

how to connect to textgrid:

browser console: 
    setSid("tgSID")
    open("tgOBJ)